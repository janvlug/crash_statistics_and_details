#!/bin/bash
echo Executing this script can take a some time, be patient...

echo First line of journal \(usually with begin and end date of log\):
journalctl | head -n 1

echo Number of oopses:
journalctl | grep -i -c "oops"

echo Number of stack traces:
journalctl | grep -i -c "stack trace"

echo Number of call traces:
journalctl | grep -i -c "call trace"

# Create file with details
FILENAME=l5-issues.`date '+%Y-%m-%d_%H%M%S'`.log
echo Find more details in this file: $FILENAME

echo Note: on the Purism forum, you can at most upload a file of 60000 characters
echo Reduce the number of days in the script if the file is too big
DAYS=5

echo Collecting logs of $DAYS days...
journalctl --since "$DAYS days ago" | grep -i -A5 -B2 "oops\|stack trace\|call trace" > ./$FILENAME

ls -l ./$FILENAME

#scp ./$FILENAME user@hostname.local:~/logdir/

